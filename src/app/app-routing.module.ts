import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BookComponent } from './book-appointment/book/book.component';
import { QueryComponent } from './query/query/query.component';
import { ViewComponent } from './view-appointment/view/view.component';
import { WelcomeComponent } from './welcome-/welcome/welcome.component';

const routes: Routes = [
  {path:"home",component:WelcomeComponent},
  {path:"book",component:BookComponent},
  {path:"view",component:ViewComponent},
  {path:"query",component:QueryComponent},
  {path:"**",component:WelcomeComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
