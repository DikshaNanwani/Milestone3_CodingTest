import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { BookComponent } from './book-appointment/book/book.component';
import { QueryComponent } from './query/query/query.component';
import { ViewComponent } from './view-appointment/view/view.component';
import { WelcomeComponent } from './welcome-/welcome/welcome.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FooterComponent } from './footer/footer.component';
import { HealthInterInterceptor } from './health-inter.interceptor';



@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    BookComponent,
    QueryComponent,
    ViewComponent,
    WelcomeComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
   

  ],
  providers: [
    {
    provide: HTTP_INTERCEPTORS,

      useClass: HealthInterInterceptor,

      multi: true,
    
  },
],
  bootstrap: [AppComponent]
})
export class AppModule { }
