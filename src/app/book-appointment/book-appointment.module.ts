import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BookAppointmentRoutingModule } from './book-appointment-routing.module';
import { BookComponent } from './book/book.component';


@NgModule({
  declarations: [
    //BookComponent
  ],
  imports: [
    CommonModule,
    BookAppointmentRoutingModule
  ]
})
export class BookAppointmentModule { }
