import { TestBed } from '@angular/core/testing';

import { HealthInterInterceptor } from './health-inter.interceptor';

describe('HealthInterInterceptor', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      HealthInterInterceptor
      ]
  }));

  it('should be created', () => {
    const interceptor: HealthInterInterceptor = TestBed.inject(HealthInterInterceptor);
    expect(interceptor).toBeTruthy();
  });
});
