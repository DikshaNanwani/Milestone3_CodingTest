import { Component, OnInit } from '@angular/core';
import { HealthServiceService } from 'src/app/Service/health-service.service';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss']
})
export class ViewComponent implements OnInit {

  constructor(private apiService:HealthServiceService) { }
  appointments:any;
  ngOnInit(): void {
    this.apiService.get().subscribe(res=>{
      this.appointments=res;
    })
  }

  trackByIndex = (index:number):number =>{
    return index;
  }

}
