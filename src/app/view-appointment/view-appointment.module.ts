import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ViewAppointmentRoutingModule } from './view-appointment-routing.module';
import { ViewComponent } from './view/view.component';


@NgModule({
  declarations: [
    ViewComponent
  ],
  imports: [
    CommonModule,
    ViewAppointmentRoutingModule
  ]
})
export class ViewAppointmentModule { }
