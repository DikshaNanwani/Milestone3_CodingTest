import { HttpClient, HttpClientModule } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';

import { HealthServiceService } from './health-service.service';

describe('HealthServiceService', () => {
  let service: HealthServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule
      ],
      providers: [HttpClient]
    });
    service = TestBed.inject(HealthServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
