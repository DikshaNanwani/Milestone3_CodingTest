import { Injectable } from '@angular/core';
import { HttpClient } from  '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class HealthServiceService {

  constructor(private http:HttpClient) { }
  url = "http://localhost:3000/appointments"

  get(){
    return this.http.get(this.url);
  }

  post(appointment:any){
    
    return this.http.post(this.url,appointment);
  }
}
